# libvirt Ansible Role

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
managing the [libvirt](https://libvirt.org/) software.
